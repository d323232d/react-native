import React from 'react';
import { TextInput, StyleSheet, View, Button } from 'react-native';
import {Formik} from 'formik';

const AddContact = ({navigation, route }) => {
  return (
    <View style={styles.container}>
      <Formik
        initialValues={{name: '', phone: '', email: '', avatar: 'https://img.icons8.com/cotton/2x/gender-neutral-user--v2.png'}}
        onSubmit={values => {
          route.params.add(values);
          navigation.goBack();
      }}>
        {props => (
          <View style={styles.form}>
            <TextInput
              style={styles.input}
              value={props.values.name}
              placeholder="Contact's name"
              onChangeText={props.handleChange('name')}/>
            <TextInput
              style={styles.input}
              value={props.values.phone}
              placeholder="Contact's phone"
              onChangeText={props.handleChange('phone')}/>
            <TextInput
              style={styles.input}
              value={props.values.email}
              placeholder="Contact's email"
              onChangeText={props.handleChange('email')}/>
            <TextInput
              style={styles.input}
              value={props.values.avatar}
              multiline
              placeholder="Contact's avatar url"
              onChangeText={props.handleChange('avatar')}/>
            <Button
              color="green"
              title="Add contact"
              onPress={props.handleSubmit}/>
          </View>
        )}
      </Formik>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  form: {
    width: '100%',
  },
  input: {
    padding: 5,
    backgroundColor: "#ffffff",
    borderWidth: 1,
    borderColor: '#d3d3d3',
    borderRadius: 5,
    marginBottom: 15,
    width: '90%',
    alignSelf: 'center',
  }
});

export default AddContact;