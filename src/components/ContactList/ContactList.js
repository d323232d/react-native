import React, { useState, useEffect, useRef } from 'react';
import {
  Image,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
  FlatList,
  Text,
  Button,
} from 'react-native';


const DATA = [
  {
    name: 'John Doe',
    phone: '88001234567',
    email: 'John@gmail.com',
    avatar: 'https://img.icons8.com/cotton/2x/gender-neutral-user--v2.png',
  },
  {
    name: 'Doe John',
    phone: '88009876543',
    email: 'Doe@gmail.com',
    avatar: 'https://img.icons8.com/cotton/2x/gender-neutral-user--v2.png',
  },
  {
    name: 'Fred Smith',
    phone: '88001111111',
    email: 'fred@gmail.com',
    avatar: 'https://img.icons8.com/cotton/2x/gender-neutral-user--v2.png',
  },  
];

const ContactList = ({ navigation }) => {
  const [contacts, setContacts] = useState([]);
  const [text, onChangeText] = useState('');

  const addContactToList = (data) => {
    for(let key in data) {
      if(!key.length) {
        return console.log(`Empty field ${key}`);
      }
    };
    setContacts(prev => {
      return [data, ...prev];
    });
  };

  const deleteContact = (id) => {
    setContacts(prev => {
      const updateList = prev.filter(cont => cont.phone !== id);
      return [...updateList];
    });
  }

  useEffect(() => {
    setContacts(DATA);
    navigation.setParams({addContactToList: addContactToList});
  }, []);

  const goToContact = item => {
    navigation.navigate('Profile', {item, del: deleteContact});
  };

  const renderItem = ({ item }) => {
    return <TouchableOpacity
        key={item.phone}
        onPress={() => goToContact(item)}
        style={[styles.row, styles.space]}>
        <Image style={styles.roundImage} source={{ uri: item.avatar }} />
        <View style={styles.container}>
          <Text>{item.name}</Text>
          <Text>{item.phone}</Text>
        </View>
      </TouchableOpacity>
  };

  const searchData = (search) => {
    const serchList = contacts.filter(item => item.name.toLowerCase().includes(search.toLowerCase()));
    return serchList;
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        value={text}
        placeholder="Search"
      />
      <Text style={styles.heading}>Contacts </Text>
      {text
      ? <FlatList
          data={searchData(text)}
          renderItem={renderItem}
          keyExtractor={item => JSON.stringify(item.phone)}/>
      : <FlatList
          data={contacts}
          renderItem={renderItem}
          keyExtractor={item => JSON.stringify(item.phone)}/>
      }

      <Button
        color='green'
        onPress={() => navigation.navigate('AddContact', {add: addContactToList})}
        title="Add contact"/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    padding: 15,
    fontWeight: 'bold',
  },
  input: {
    width: '90%',
    margin: 15,
    padding: 15,
    alignSelf: 'center',
    borderColor: '#d3d3d3',
    borderWidth: 1,
    borderRadius: 50,
    fontSize: 16,
  },
  roundImage: {
    width: 40,
    height: 40,
    margin: 10,
    borderRadius: 50,
    backgroundColor: '#d3d3d3',
  },
  row: {
    flexDirection: 'row',
  },
  space: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  
});

export default ContactList;
