import React from 'react';
import { Text, StyleSheet, View, Image, Button } from 'react-native';
import { Linking } from 'react-native';

const Profile = (props) => {
  const info = props.route.params.item;
  return (
    <View style={{flex: 1}}>
      <View style={styles.container}>
        <Image style={styles.roundImage} source={{ uri: `${info.avatar}` }} />
        <Text style={styles.name}>{info.name}</Text>
        <Text style={styles.info}>Phone: {info.phone}</Text>
        <Text style={styles.info}>Email: {info.email}</Text>
      </View>
      <View style={styles.fixToText}>
        <Button
          color='green'
          onPress={() => { Linking.openURL(`tel:${props.phone}`) }}
          title="               Call                 " />
        <Button
          color='red'
          onPress={() => { 
            console.log("ups");
            props.route.params.del(info.phone);
            props.navigation.goBack();
          }}
          title="          Delete          " />
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  roundImage: {
    width: 100,
    height: 100,
    margin: 10,
    borderRadius: 50,
    backgroundColor: '#d3d3d3',
  },
  name: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20
  },
  info: {
    marginBottom: 15,
    textAlign: 'left',
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
});

export default Profile;