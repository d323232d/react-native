import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ContactList from '../../components/ContactList/ContactList';
import Profile from '../../components/Profile/Profile';
import AddContact from '../../components/AddContact/AddContact';

const Stack = createStackNavigator();

export default function SearchNavigator({navigation}) {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Search"
          component={ContactList}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{
            title: 'Profile',
          }}
        />
        <Stack.Screen
          name="AddContact"
          component={AddContact}
          options={{
            title: 'New contact',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
};
