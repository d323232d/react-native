/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
// import 'react-native-gesture-handler';
import React from 'react';

import {SearchNavigator} from './src/routes';
import {StyleSheet, SafeAreaView, View } from 'react-native';
import ContactList from './src/components/ContactList/ContactList';

const App = () => {
  return (
    <SearchNavigator />
    // <SafeAreaView style={styles.container}>
    //   <ContactList />
    // </SafeAreaView>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
